import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStreamReader;

public class PemTest {
  @Test
  public void test() throws Exception {
    ClassPathResource resource = new ClassPathResource("prikey.pem");
    String priKey = null;
    try {
      InputStreamReader reader = new InputStreamReader(resource.getInputStream(), "UTF-8");
      char[] chars = new char[1024];
      StringBuilder sb = new StringBuilder();
      while (true) {
        int rsz = reader.read(chars);
        if (rsz < 0) break;
        sb.append(chars, 0, rsz);
      }
      priKey = sb.toString();
      System.out.println(priKey);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
