package com.springboot.oauth2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.springboot.oauth2.mapper")
@SpringBootApplication
public class AuthorizationServer {
  public static void main(String[] args) {
    SpringApplication.run(AuthorizationServer.class);
  }
}
