package com.springboot.oauth2.mapper;

import com.springboot.oauth2.entity.Login;

public interface LoginMapper {
  Login selectByUsername(String username);
}
