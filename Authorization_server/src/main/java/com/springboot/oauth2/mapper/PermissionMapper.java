package com.springboot.oauth2.mapper;


import com.springboot.oauth2.entity.Permission;

import java.util.List;

public interface PermissionMapper {
  List<Permission> listPermissions();

  List<Permission> listByUserId(Long id);
}
