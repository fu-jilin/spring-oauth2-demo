package com.springboot.oauth2.entity;

import lombok.Data;

@Data
public class Permission {
	private Long id;
	private String resource;
	private String sn;
}
