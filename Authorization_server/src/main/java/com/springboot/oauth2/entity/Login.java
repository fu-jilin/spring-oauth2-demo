package com.springboot.oauth2.entity;

import lombok.Data;

@Data
public class Login {
  private Long id;
  private String name;
  private String password;
  private String phone;
}
