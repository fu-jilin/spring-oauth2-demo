import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

public class JwtVarifyTest {
  // keytool -export -alias whalechen -keystore xxx.jks -storepass whalechen -file xxx.cer
  public static void main(String[] args) {
    String token =
        "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsicmVzMSJdLCJ1c2VyX2lkIjoxLCJwaG9uZSI6IjEyMzQ1Njc4ODg4ODg4IiwidXNlcl9uYW1lIjoienMiLCJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjQwMjMwMTA2LCJhdXRob3JpdGllcyI6WyJkZXB0OmFkZCIsImRlcHQ6bGlzdCIsIlJPTEVfZGVsZXRlIl0sImp0aSI6IjNkM2ExODY1LWE2MmMtNGYxOS1iODJjLWNhZmE4N2ZiODdiMCIsImNsaWVudF9pZCI6IndlYmFwcCJ9.uPpjf72-84F3kVvEehL8KiX2hIfHe0ZidA83Mb3cgYmLEta0lbatnwqwHd3hlRo1CaQjm4rxdDIyMx0AsqK6qaUOc6Vx_Z_FVObrl0eUrCoPnJn8Jze5B-tQxUnOiE8pXXmSsMTg85px0XGRDQXFjxled0tD0SO4TX0lPFRQWZEq47MDZl6TqRAD8bP33W89MmxSfUP70A50unEzP1pnho2Fdqajh2nurbrJ6XaCAU5a4RhxkThnMjggZ03pjB4KQ_ye_FsQeDT-oDaoZOs5J-pXJmV3oWFxJm9iluvBixWrY8v0DJ-2eCMwWi8S-38eiRtqc_FNYDJ_tszOIU7dMg"
            + "";
    try {
      ClassPathResource resource = new ClassPathResource("pubKey.cer");
      PublicKey publicKey =
          CertificateFactory.getInstance("X.509")
              .generateCertificate(resource.getInputStream())
              .getPublicKey();
      Claims claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();
      String phone = claims.get("phone", String.class);
      System.out.println("成功！！手机号：" + phone);
    } catch (ExpiredJwtException e) {
      System.out.println("token超时");
      System.out.println(e.getMessage());
    } catch (CertificateException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SignatureException e) {
      System.out.println("验签失败");
      e.printStackTrace();
    }
  }
  @Test
  public void testPasswordEncode() throws Exception {
    String encode = new BCryptPasswordEncoder().encode("123");
    System.out.println(encode);
  }
}
