package com.springboot.oauth2.intercepter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class WebConfig implements WebMvcConfigurer {
  @Autowired WebInterceptor webIntercepter;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry
        .addInterceptor(webIntercepter)
        // 配置拦截的路径，这里所有路径都会拦截
        .addPathPatterns("/**");
    // 排除拦截
    // .excludePathPatterns()
  }
}
