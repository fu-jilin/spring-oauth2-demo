package com.springboot.oauth2.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

  @RequestMapping("/dept/list")
  @PreAuthorize("hasAuthority('dept:list')")
  public String list() {
    return "dept.list";
  }

  @RequestMapping("/dept/add")
  @PreAuthorize("hasAuthority('dept:add')")
  public String add() {
    return "dept.add";
  }

  @RequestMapping("/dept/update")
  @PreAuthorize("hasAuthority('dept:update')")
  public String update() {
    return "dept.update";
  }

  @RequestMapping("/dept/delete")
  // @PreAuthorize("hasAuthority('dept:delete')")
  @Secured("ROLE_delete")
  public String delete() {
    return "dept.delete";
  }

  @RequestMapping("/show/{id}")
  @PreAuthorize("isAnonymous()")
  public String show(@PathVariable("id") Long id) {
    return "部门" + id + " 信息：不存在";
  }
}
