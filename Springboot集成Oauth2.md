# 一. 环境准备

## 1.1 数据库准备

项目下/doc/mysql/oauth2demo.mysql文件使用Navicat导入数据库

## 1.2 授权服务器

### 1.2.1 maven配置



### 1.2.2 yml配置

### 1.2.3 连接Mysql，配置Mybatis

## 1.3 资源服务器

### 1.3.1 maven配置

### 1.3.2 yml配置

### 1.3.3 编写Controller

# 二.授权服务器配置

## 2.1 集成Oauth2

## 2.2 Security配置

## 2.3 授权服务器配置



# 三. 资源服务器配置

## 3.1 集成Oauth2

## 3.2 Security配置

## 3.3 资源服务器配置



# 四. 进阶内容

## 4.1 自定义Jwt内容

## 4.2 Jwt实现非对称加密

## 4.3 解析Jwt内容